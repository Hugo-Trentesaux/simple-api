import sys

if len(sys.argv) == 1: # if no arguments given load empty
    prerun = 'empty.py'
else: # else, load given file
    prerun = sys.argv[1]

# import server class
from main.server import run

# creates the scope of execution
SCOPE = {}
with open(prerun) as fid:
    exec(fid.read(), SCOPE)

# starts the server
run(port=8000, scope=SCOPE)
