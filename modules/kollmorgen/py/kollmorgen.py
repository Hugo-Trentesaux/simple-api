from serial import Serial
from serial.serialutil import SerialException
from time import sleep
from math import floor, copysign

# serial parameters
PORT = "COM3"
BAUDRATE = 38400

CMD_PAUSE = 0.020 # wait 20 ms after sending message (10 ms is not enough)
J_DEGPS = 10 # "J" value for 1 degree per sec (e.g. 100 °/s -> J 1000) (J is rpm)

# Internal values, see motor doc to understand detail
PRBASE = 8 
PGEARI = 60
PGEARO = 1
# lead to 
RES = PGEARI / PGEARO / 6 # number returned at 1°

# invert the sign if you want to change the direction of positive angles and speed
# /!\ does not affect the text you write (only core functions) 
SIGN = -1
    
class Kollmorgen():
    """ doc is here https://github.com/LaboJeanPerrin/wiki/blob/master/RLS-tilt/KollmorgenManual.md
    vars:
    s is a serial.Serial instance """
    
    def __init__(self):
        """ initialize serial connection and motor internal parameters """
        # min and max angles are used in safe version of functions
        self.minAngle = -20 # degrees
        self.maxAngle = 20 # degrees
        self.defaultSpeed = 20 # degrees per second (positive value)
        try:
            self.s = Serial(port=PORT, baudrate=BAUDRATE)
        except SerialException:
            raise SerialException("can not connect to the motor, please make sure no connection is already alive")
        self._setDefault()
        
    def write(self, text):
        """ convert text to ascii and write it plus end line to serial """
        self._writeAscii(text.encode('ascii') + b'\r\n')
        sleep(CMD_PAUSE) 
        
    # base write commands
    def clrfault(self):
        self.write("CLRFAULT")
    def coldstart(self):
        self.write("COLDSTART")
    def enable(self):
        self.write("EN")
    def disable(self):
        self.write("DIS")
    def zero(self):
        self.write("SETREF")
    def pos(self): # returns position in degree
        self._clearOutput() # make sure no byte remains
        self.write("PFB")
        return self._readNumber()/RES
    
    # base read commands
    def reset(self):
        self.clrfault()
        sleep(3)
        self._setDefault()
        self.enable()
        return "ok"
    def readAll(self): # returns a byte array 
        return self.s.read_all()
    def echo(self):
        s = self.readAll()
        print(s)
        return s
    def writeAndRead(self, text):
        self.write(text)
        return self.readAll()		
        
    # level 1 base functions
    def rotate(self, speed): # rotation speed in degree / sec (with sign)
        self._j(floor(speed * J_DEGPS))
    def moveRel(self, angle, speed=None): # relative angle, absolute speed
        if speed == 0:
            self.stop()
            return
        if angle: # a relative movement of zero moves nothing
            if speed is None:
                speed = self.defaultSpeed
            speed = copysign(speed, angle) # if negative angle, then negative speed
            time = abs(angle / speed) # time in seconds necessary for the movement
            self._jj(floor(speed * J_DEGPS), floor(time*1000)) # relative movement at the given speed
        
    # higher level base functions
    def stop(self):
        self.rotate(0)
    def moveAbs(self, target, speed=None): # measure the current position and do the computed relative movement
        current = self.pos()
        self.moveRel(target - current, speed=speed)
    def home(self):
        self.moveAbs(0)
        
    # "safe" functions that make sure the motor does not exceed maximal positions
    # and that the input or output buffer of the serial is clean (empty)
    def safeRotate(self, speed):
        target = self.maxAngle if speed > 0 else self.minAngle # reach limit
        # self._clearSerial()
        self.moveAbs(target, speed=speed)
    def safeMoveAbs(self, target, speed=None):
        # self._clearSerial()
        if target < self.minAngle:
            self.moveAbs(self.minAngle, speed=speed)
        elif target > self.maxAngle:
            self.moveAbs(self.maxAngle, speed=speed)
        else: # self.minAngle < target < self.maxAngle
            self.moveAbs(target, speed=speed)
    def safeMoveRel(self, angle, speed=None):
        # self._clearSerial()
        current = self.pos()
        target = current + angle
        # AVOID self.safeMoveAbs(target, speed=speed) BECAUSE position would be measured twice
        if target < self.minAngle:
            self.moveRel(self.minAngle-current, speed=speed)
        elif target > self.maxAngle:
            self.moveRel(self.maxAngle-current, speed=speed)
        else: # self.minAngle < target < self.maxAngle
            self.moveRel(angle, speed=speed)
    
    # internal class functions
    def _writeAscii(self, ascii):
        self.s.write(ascii)
    def _clearInput(self):
        self._writeAscii(b'\r\n')
    def _clearOutput(self):
        self.readAll()
    def _clearSerial(self):
        # pass
        self._clearInput()
        sleep(CMD_PAUSE) 
        self._clearOutput()
    def _readNumber(self):
        r = self.readAll()
        parts = r.split(b'\r\n')
        if len(parts) != 2:
            raise SerialException(r)
        string = parts[-2].decode('ascii')
        integer = int(string)
        return SIGN*integer
    def _j(self, speed): # speed is in rpm (integer)
        self.write("J " + str(SIGN*speed))
    def _jj(self, speed, duration): # speed in rpm, duration in ms (both integer)
        self.write("J " + str(SIGN*speed) + " " + str(duration))
        
    def _setDefault(self):
        self.write("PROMPT 0") # avoid returning "-->"
        self.write("PRBASE " + str(PRBASE))
        self.write("PGEARI " + str(PGEARI))
        self.write("PGEARO " + str(PGEARO))
        self._clearOutput()
        
