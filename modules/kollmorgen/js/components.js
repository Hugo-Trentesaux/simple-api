// only get value
function getVal(variable) {
  return function(e) {
    getDict()
  }
}
// only set value
function setTo(variable) {
  return function(e) {
    DICT[variable] = e.target.value
  }
}

// do function if defined, elso do nothing
function doOrNot(fun) {
    if(!(typeof(fun) === "undefined")) { fun() }
}

function getComponent(type, variable, args) {
	switch (type) {
	// simple label
	case "display":
		return [
			m("label", {}, variable + ": " + DICT[variable] )
		]
	break
    // editable text element
    case "text":
       return [
        m("label", {}, variable + ": " + DICT[variable] ),
        m("br"),
        m("input[type=text]", {value: DICT[variable], oninput: setTo(variable) })
      ]
    break
    // editable range element
    case "range":
      return [
        m("label", {}, variable + ": " + DICT[variable] ),
        m("br"),
        m("input[type=range]", {min:args.min, max:args.max, value: DICT[variable],
            onchange: function(e) { doOrNot(args.onchange) }, 
            oninput: function(e) { setTo(variable)(e); doOrNot(args.oninput) } }),
      ]
    break
	// button
	case "button":
	  return [
	  	m("button", {onclick: args}, variable),
	  ]
	break
	// number
	case "number":
	  return [
	  	m("input[type=number]", {value: DICT[variable], oninput: setTo(variable)}),
	  ]
	break
  }
}

// function to mount component of a given type, variable name and optionnal args
function mount(type, selector, variable, args) {
  root = document.querySelector(selector)
  var component = {view: function() { return getComponent(type, variable, args) }}
  m.mount(root, component)
}



function setAction(selector, fun) {
    b = document.querySelector(selector)
    b.onclick = fun
}