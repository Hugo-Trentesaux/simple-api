// ask command
function send(command) {
  m.request({
    method: "POST",
    url: URL,
    data: {type: 'code', code:command},
  })
  .then( function(result) {
    DICT.message = result.result
  })
}
function asciiCmd(cmd) { send( "print(k.writeAndRead( '"+cmd+"' ))" ) }

function enable() { send("k.enable()") }
function disable() { send("k.disable()") }
function zero() { send("k.zero()") }
function stop() { send("k.stop()") }
function home() { send("k.home()") }
function reset() { send("k.reset()") }
function readAll() { send("k.readAll()") }
function moveRel(angle) { send( "k.safeMoveRel(" + angle + ")" ) }
function moveAbs(target) { send( "k.safeMoveAbs(" + target + ")" ) }
function rotate(speed) { send( "k.safeRotate(" + speed + ")" ) }

function pos() { send( "print(k.pos())" ) }

function load(protocol) { send( "p.loadfile('" + protocol + "')" ) }
function runprotocol() { send( "p.run({'k':k})" ) } // not used here
function startprotocol() { send( "T = startProtocol(p, globals())" ) }
function killprotocol() { send( "kill(T)" ) }
