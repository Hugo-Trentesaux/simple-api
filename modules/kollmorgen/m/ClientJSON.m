classdef ClientJSON < handle
    properties        
        address
    end
    properties (Hidden)
        header = [matlab.net.http.field.AcceptField(matlab.net.http.MediaType('application/json'))...
            matlab.net.http.field.ContentTypeField('text/plain');];
    end
    methods
        function self = ClientJSON()
            self.address = 'localhost:8000';
        end
        function result = GET(self)
            request = matlab.net.http.RequestMessage;
            response = send(request, self.address);
            result = response.Body.Data;
        end
        function result = POST(self, data)
            body = jsonencode(data);
            request = matlab.net.http.RequestMessage(matlab.net.http.RequestMethod.POST,...
                self.header,...
                body);
            response = send(request, self.address);
            result = response.Body.Data;
            if result.status
                error(result.result)
            end
        end
    end
end