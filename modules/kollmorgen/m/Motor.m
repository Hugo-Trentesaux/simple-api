classdef Motor < ClientJSON
    % the class motor allows to control the Kollmorgen motor from Matlab if a server is active
    % (see https://github.com/LaboJeanPerrin/wiki/blob/master/RLS-tilt/KollmorgenManual.md)
    % basic commands are :
    % enable, disable, zero, stop, home, moveRel, moveAbs, rotate, pos
    properties
    end
    methods
        function self = Motor()
            % constructor
            self = self@ClientJSON();
        end
        function result = send(self, command)
            % sends command and arguments from structure
            data.type = "code";
            data.code = command;
            result = self.POST(data);
        end

        % basic commands
        function enable(self); self.send("k.enable()"); end
        function disable(self); self.send("k.disable()"); end
        function zero(self); self.send("k.zero()"); end
        function stop(self); self.send("k.stop()"); end
        function home(self); self.send("k.home()"); end
        function moveRel(self, angle); self.send(["k.safeMoveRel(" num2str(angle) ')']); end
        function moveAbs(self, target); self.send(["k.safeMoveAbs(" num2str(target) ')']); end
        function rotate(self, speed); self.send(["k.safeRotate(" num2str(speed) ')']); end

        % read position
        function pos = pos(self)
            result = self.send("print(k.pos())");
            pos = str2int(result.result);
        end
    end
end
