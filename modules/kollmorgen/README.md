# Kollmorgen motor controller

The basic architecture of the program is the following:

```text
motor controller
      | (serial connection, ascii commands)
python wrapper
      | (python calls)
python server ------------ ------------------
      |         OR        |         OR       |   (http GET and POST requests)
web interface       matlab client           ...
```

This module uses it to interface the kollmorgen motor.