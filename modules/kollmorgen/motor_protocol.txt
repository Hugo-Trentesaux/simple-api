# preparation phase
= 0 : k.enable()
+ 0 : k.safeMoveAbs(-5)

# start oscillation
= 1 : k.safeRotate(10)
+ 1 : k.safeRotate(-10)
+ 1 : k.safeRotate(10)
+ 1 : k.safeRotate(-10)
+ 1 : k.safeRotate(10)
+ 1 : k.safeRotate(-10)
+ 1 : k.safeRotate(10)
+ 1 : k.stop()

# finish
+ 2 : k.home()
+ 1 : k.disable()
- 0.1 : print("finished")