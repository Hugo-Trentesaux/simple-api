# Protocol

Protocol is a very simple python program that allows you to run a schedule based on a text file. With the following format :

```
# syntax :
# =======
# 1. sign
#     = absolute
#     + relative after
#     - relative before
# 2. time in seconds
# 3. column sign
# 4. python command
# =======
# ligns beginning with sharp are comments
# empty ligns are ignored
# incorrect line generate warning

= 0 : print("hello")
+ 0.1 : print("it's me")
+ 0.1 : print("I am having fun")

= 1 : print("the end")
```

​Each line gives an absolute timing or a timing relative to the last command.

​	