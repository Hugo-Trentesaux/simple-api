import threading
import ctypes

def runInThread(fn):
    t = threading.Thread(target=fn)
    t.start()
    return t.ident
    
def startProtocol(p, glo):
    return runInThread(lambda : p.run(glo))
    
def kill(tid):
    ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, ctypes.py_object(InterruptedError))
    print(f"thread {tid} killed")
    
def wrapProtocol(p): # wraps the protocls commands in a request to avoid command conflicts
    for i,line in enumerate(p.lines): # for each line of the protocol
        p.lines[i] = """requests.post(\
"http://localhost:8000/", \
data='{"type": "code", "code": " """ + line + """ "}')\
"""
    # not used yet because requests is slow on windows (?)