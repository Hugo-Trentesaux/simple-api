import re
import sched, time

class Protocol():
    """ parse a text file and provide an iterator amid lines """
    IGNORED = { # a set of regexps describing ligns to ignore
        r"^\s*$", # empty line
        r"^#", # comment line
    }
    PATTERN =  r"\s*([\=\-\+])\s*(\d+\.?\d*)\s*:\s*(.*)" # regexp for a correct line

    def __init__(self): # contructor, init properties
        self.lines = []
        self.protocol = None

    def loadfile(self, filename): # load lines from a file
        with open(filename) as fid:
            self.loadlines(fid.readlines())

    def loadtext(self, text): # load lines from a text (string)
        self.loadlines(text.splitlines())

    def loadlines(self, lines): # load lines in instance
        self.lines = lines
        self.schedule()

    def __iter__(self): # allow to iterate over lines
        self.i = 0 # line number
        return self

    def __next__(self): # iterates over valid lines
        if self.i >= len(self.lines):
            raise StopIteration
        line = self.lines[self.i]
        p = self.parsed(line)
        self.i += 1 # increment
        if not p: # if ignored
            return self.__next__()
        return p

    def parsed(self, line):
        """ returns a tuple (sign, value, command) or None if ignored"""
        for reg in self.IGNORED:
            if re.match(reg, line):
                return None
        m = re.match(self.PATTERN, line)
        if not m:
            raise Exception(f"line {line} does not match pattern {self.PATTERN}")
        return (m.group(1), float(m.group(2)), m.group(3))

    def schedule(self):
        """ read lines from a parser and provide an iterator with absolute time and python commands """
        schedule = [] # a list that will contain all commands with absolute timing
        t = 0 # current time
        for line in self: # fills the list with relative timings
            if line[0] == '=':
                t = line[1]
            elif line[0] == '+':
                t += line[1]
            elif line[0] == '-':
                t -= line[1]
            schedule.append((t, line[2]))
        schedule = sorted(schedule, key = lambda e : e[1])
        self.protocol = schedule

    def run(self, globals=None):
        """ precision on beginning time might slightly depend on protocol size """
        s = sched.scheduler()
        for entry in self.protocol:
            s.enter(entry[0], 1, exec, argument=(entry[1], globals))
        s.run()
