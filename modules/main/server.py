from http.server import * # http server tools
import json # "JavaScript Object Notation" to communicate with programs
import sys
from io import StringIO
import contextlib

SCOPE = {}

@contextlib.contextmanager
def switchstdout():
    sys_stdout = sys.stdout
    new_stdout = StringIO()
    sys.stdout = new_stdout
    yield new_stdout # let it go
    sys.stdout = sys_stdout # set it back

def interpret(dic): # decide what the user wants to do based on the dict
    if not type(dic) is dict:
        raise Exception("Sent data is not properly formated (not dict)")
    if not "type" in dic:
        raise Exception("Request is not properly formated (no type)")
    request = dic['type']
    # TODO allow to define custom request type in the modules themselves
    if request == "code":
        code = dic['code']
        with switchstdout() as s: # allows to get printed value from execution
            exec(code, SCOPE)
    elif request == "eval": # only for eval, do not accept statements
        code = dic['eval']
        return eval(code, SCOPE)
    elif request == "update":
        data = dic['dict']
        SCOPE['DICT'].update(data)
        return SCOPE['DICT']
    else:     
        raise Exception(f"Request type not accepted: {request}")

class Responder(BaseHTTPRequestHandler):
    """ class responsible of response handling """

    # allow pre-fetch query
    def do_OPTIONS(self):
        # print("OPTIONS")
        self.send_response(200)
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.send_header('Access-Control-Allow-Headers', 'X-PINGOTHER, Content-Type')
        self.end_headers()

    # get is not implemented here
    def do_GET(self):
        # print("GET")
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
        data = {'message': "GET requests are not implemented yet (could return interface or state of variables)"}
        self.wfile.write(json.dumps(data).encode('utf8'))

    # send command to motor with JSON content
    def do_POST(self):
        # print("POST")
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
        data = self.rfile.read(int(self.headers['Content-Length']))
        dic = json.loads(data)
        print(dic) # prints the request to the command prompt
        try:
            status = 'success'
            result = interpret(dic)
        except Exception as e:
            status = 'failure'
            result = repr(e)

        if isinstance(result, bytes): # result can be bytes, then decode it
            result = result.decode('ascii') # todo shift this to method

        data = {'status': status, 'result': result}
        self.wfile.write(json.dumps(data).encode('utf8')) # returns

# for multiprocess, use ThreadingHTTPServer instead of HTTPServer
def run(server_class=HTTPServer, handler_class=Responder, port=8000, scope={}):
    global SCOPE # the scope of execution
    SCOPE = scope
    server_address = ('localhost', port)
    httpd = server_class(server_address, handler_class)
    sa = httpd.socket.getsockname()
    serve_message = "Serving HTTP on {host} port {port} (http://{host}:{port}/) ..."
    print(serve_message.format(host=sa[0], port=sa[1]))
    httpd.serve_forever()

if __name__ == "__main__":
    run()
