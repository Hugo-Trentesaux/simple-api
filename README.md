# Simple API

This application is a python http server compatible with browser requests. It allows to interact with a python program from any client using JSON. The client can be a javascript program running as a visual interface in the browser as well a Matlab program or even a python one.

The API can run any python code and can be easily extended.

**Install**

Just clone this git

**Use**

Start the python server (python 3.7 required for multi-threading server). You can edit the prerun.py file to make variables accessible in the scope.

    cd simple-api/modules/
    python start.py

Open the main/demo.html in your browser to see the demo. It should look like this:

![screenshot](./doc/img/screenshot.png)

You can both write code and execute it, or execute single line.

**Kollmorgen module**

This module allows to control the Kollmorgen motor with user friendly functions build on serial communication.

**Thanks**

- [vue.js](https://vuejs.org/)
- [python http.server](https://docs.python.org/3/library/http.server.html)
